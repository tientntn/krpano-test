import React, { useEffect, useState, useRef } from "react";
import Head from "next/head";
// import Krpano from 'react-krpano';
import dynamic from "next/dynamic";
const Krpano = dynamic(() => import("react-krpano"), { ssr: false });

const Index = () => {
  const [isLoaded, setIsLoaded] = useState(false);
  useEffect(() => [setIsLoaded(1)], []);
  return (
    <>
      <Head>
        <script async src="/js/jquery.min.js" />
        <script async src="/krpano/tour.js" />
        <script async src="/js/vr360.js" />
      </Head>
      <Krpano xml="/krpano/tour.xml" />
    </>
  );
};

export default Index;
