import React, { useEffect, useState, useRef } from 'react';
import Head from 'next/head';
import Krpano from 'react-krpano';
const Index = () => {
  const [isLoaded, setIsLoaded] = useState(false);
  useEffect(() => [setIsLoaded(1)], []);
  return (
    <>
      <Head>
        <script async src="/krpano/tour.js" />
      </Head>
      {isLoaded && <Krpano xml="/krpano/tour.xml" />}
    </>
  );
};

export default Index;
